export const average = (numbers: number[]): number => {
  return numbers.reduce(function(a, b){ return a + b; })/numbers.length;
};
