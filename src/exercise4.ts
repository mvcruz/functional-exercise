const _wordCount = (words: string[], index: number, record: Record<string, number>): Record<string, number> => {
  if (words.length == index) {
    return record
  }
  let count: number = 0;
  if (Object.keys(record).includes(words[index])) {
    count = record[words[index]]
  }
  record[words[index]] = count + 1
  return _wordCount(words, index + 1, record)
}

export const wordCount = (sequence: string): Record<string, number> => {
  if (sequence.trim().length == 0)
    return {}
  const wordsCounted: Record<string, number> = {};
  const lowerCased = sequence.split(" ").map(sequence => sequence.toLowerCase());
  return _wordCount(lowerCased, 0, wordsCounted);
};
